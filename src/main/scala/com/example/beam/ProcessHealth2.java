package com.example.beam;

import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.io.TextIO;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.transforms.DoFn;
import org.apache.beam.sdk.transforms.GroupByKey;
import org.apache.beam.sdk.transforms.MapElements;
import org.apache.beam.sdk.transforms.ParDo;
import org.apache.beam.sdk.values.KV;
import org.apache.beam.sdk.values.PCollection;
import org.apache.beam.sdk.values.TypeDescriptors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Iterator;

public class ProcessHealth2 {

    private static final Logger LOG = LoggerFactory.getLogger(ProcessHealth2.class);


    public static void main(String[] args) {
        AppOptions options = PipelineOptionsFactory.fromArgs(args).withValidation().as(AppOptions.class);

        runProcess(options);
    }

    private static void runProcess(AppOptions options) {
        AWSOptions.formatOptions(options);
        Pipeline pipeline = Pipeline.create(options);
        PCollection<String> input = pipeline.apply(TextIO.read().from(options.getBucketUrl()));
        LOG.info("Running wordcount example from ${options.getBucketUrl} to ${options.getOutputPath} ...");

        input.apply(ParDo.of(new Q1Obj())).apply(GroupByKey.create())
                .apply(ParDo.of(new DoFn<KV<String, Iterable<Double>>, KV<String, Double>>() {
                    @ProcessElement
                    public void processElement(ProcessContext c) {
                        String key = c.element().getKey();
                        Iterator<Double> iter = c.element().getValue().iterator();

                        Double bigvalue = 0.0;
                        int count = 0;
                        while (iter.hasNext()) {
                            count++;
                            bigvalue = bigvalue + iter.next();
                        }

                        bigvalue = bigvalue / count;

                        c.output(KV.of(key, bigvalue));
                    }
                })).apply("FormatResults", MapElements
                .into(TypeDescriptors.strings())
                .via((KV<String, Double> wordCount) -> wordCount.getKey() + ": " + wordCount.getValue()))
                .apply(TextIO.write().to(options.getOutputPath() + "/avgcovered"));

        input.apply(ParDo.of(new DoFn<String, KV<String, Double>>() {
            @ProcessElement
            public void processElement(ProcessContext c) {
                String[] strArr = c.element().split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
                c.output(KV.of(strArr[5], new Double(strArr[11])));

            }
        })).apply(GroupByKey.create())
                .apply(ParDo.of(new DoFn<KV<String, Iterable<Double>>, KV<String, Double>>() {
                    @ProcessElement
                    public void processElement(ProcessContext c) {
                        String key = c.element().getKey();
                        Iterator<Double> iter = c.element().getValue().iterator();

                        Double bigvalue = 0.0;
                        int count = 0;
                        while (iter.hasNext()) {
                            count++;
                            bigvalue = bigvalue + iter.next();
                        }

                        bigvalue = bigvalue / count;

                        c.output(KV.of(key, bigvalue));
                    }
                })).apply("FormatResults", MapElements
                .into(TypeDescriptors.strings())
                .via((KV<String, Double> wordCount) -> wordCount.getKey() + ": " + wordCount.getValue()))
                .apply(TextIO.write().to(options.getOutputPath() + "/avgmedicare"));


        input.apply(ParDo.of(new DoFn<String, KV<String, Double>>() {
            @ProcessElement
            public void processElement(ProcessContext c) {
                String[] strArr = c.element().split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
                c.output(KV.of(strArr[5] + ":" + strArr[0], new Double(strArr[11])));

            }
        })).apply(GroupByKey.create())
                .apply(ParDo.of(new DoFn<KV<String, Iterable<Double>>, KV<String, Double>>() {
                    @ProcessElement
                    public void processElement(ProcessContext c) {
                        String key = c.element().getKey();
                        Iterator<Double> iter = c.element().getValue().iterator();

                        Double bigvalue = 0.0;
                        int count = 0;
                        while (iter.hasNext()) {
                            count++;
                            bigvalue = bigvalue + iter.next();
                        }
                        c.output(KV.of(key, bigvalue));
                    }
                })).apply("FormatResults", MapElements
                .into(TypeDescriptors.strings())
                .via((KV<String, Double> wordCount) -> wordCount.getKey() + ": " + wordCount.getValue()))
                .apply(TextIO.write().to(options.getOutputPath() + "sumdischarge"));

        pipeline.run().waitUntilFinish();

    }

}
