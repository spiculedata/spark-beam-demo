# Spark - Beam - Demo

I've created this little POC for a demo I had been asked to do. It's a quick look at Apache Beam designed to be compatible with the Spark runner and download and upload data to S3.

It was also designed to be run directly against a Kubernetes cluster via Spark Submit.

## To test it you can run it standalone

    mvn clean scala:run -DmainClass=com.example.beam.ProcessHealth2 \
    -DaddArgs="--awsKey=<AWS KEY>|-awsSecret=<AWS SECRET>|--outputPath=s3://demobucket/|--awsRegion=us-east-1"

## To run via Spark Submit

    mvn package

and pass the resulting file to spark-submit.

## To run via Spark Submit on Kubernetes!

Firstly you need to have built a Spark Docker image with your jar in.

    sudo ./bin/docker-image-tool.sh -r <repo namespace> -t <tag> build
    sudo ./bin/docker-image-tool.sh -r <repo namespace> -t <tag> push

Then

    ./spark-submit --master k8s://https://<k8s api url>:6443 --deploy-mode cluster --name spark-demo --class com.example.beam.ProcessHealth2 --conf spark.executor.instances=5 --conf spark.kubernetes.authenticate.driver.serviceAccountName=spark  --conf spark.kubernetes.container.image=<your>/<docker>:<image> local:///opt/wordcount-app-1.0.0-shaded.jar "--runner=SparkRunner" "--awsKey=<AWS Key>" "--awsSecret=<AWS Secret>" "--outputPath=s3://demobucket/" "--awsRegion=us-east-1"
