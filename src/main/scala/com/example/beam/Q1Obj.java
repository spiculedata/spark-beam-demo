package com.example.beam;

import org.apache.beam.sdk.transforms.DoFn;
import org.apache.beam.sdk.values.KV;

public class Q1Obj extends DoFn<String, KV<String, Double>> {

    private static final long serialVersionUID = 1L;

    @ProcessElement
    public void processElement(ProcessContext c) {
        String[] strArr = c.element().split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
        c.output(KV.of(strArr[5], new Double(strArr[9])));
    }


}
