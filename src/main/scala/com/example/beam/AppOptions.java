package com.example.beam;

import org.apache.beam.sdk.io.aws.options.S3Options;
import org.apache.beam.sdk.options.Default;
import org.apache.beam.sdk.options.Description;
import org.apache.beam.sdk.options.PipelineOptions;
import org.apache.beam.sdk.options.Validation;

public interface AppOptions extends PipelineOptions, S3Options {

    @Description("Output path")
    @Validation.Required
    String getOutputPath();

    void setOutputPath(String value);

    @Description("AWS region")
    String getAwsRegion();

    void setAwsRegion(String value);

    @Description("AWS Key")
    String getAwsKey();

    void setAwsKey(String value);

    @Description("AWS Secret")
    String getAwsSecret();

    void setAwsSecret(String value);

    @Description("Bucket URL")
    @Default.String("s3://canonicaldemo/inpatientCharges.csv")
    String getBucketUrl();

    void setBucketUrl(String value);
}
